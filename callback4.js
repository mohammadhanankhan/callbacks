/* 
	Problem 4: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind list simultaneously
*/
const callback1 = require('./callback1');
const callback2 = require('./callback2');
const callback3 = require('./callback3');

const callback4 = function (
  { boards, lists, cards, boardName, listName },
  callback
) {
  if (
    Array.isArray(boards) &&
    typeof lists === 'object' &&
    lists !== null &&
    typeof cards === 'object' &&
    cards !== null &&
    typeof boardName === 'string' &&
    typeof listName === 'string' &&
    typeof callback === 'function'
  ) {
    setTimeout(function () {
      const boardId = boards
        .filter(board => {
          return board.name === boardName;
        })
        .map(board => board.id)
        .join();

      callback1(boards, boardId, (err, boardData) => {
        if (err) {
          console.error(err);
        } else {
          console.log(boardData);

          callback2(lists, boardData.id, (err, listData) => {
            if (err) {
              console.error(err);
            } else {
              console.log(listData);

              const list = listData.find(list => list.name === listName);

              if (list !== undefined) {
                callback3(cards, list.id, (err, data) => {
                  if (err) {
                    console.error(err);
                  } else {
                    console.log(data);
                  }
                });
              }
            }
          });
        }
      });
    }, 2 * 1000);
  } else {
    callback(new Error('Invalid Input!'));
  }
};

module.exports = callback4;
