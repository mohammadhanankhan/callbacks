const boards = require('../data/boards.json');
const lists = require('../data/lists.json');
const cards = require('../data/cards.json');

const callback6 = require('../callback6');

const boardName = 'Thanos';

callback6(
  {
    boards,
    lists,
    cards,
    boardName,
  },
  err => {
    console.error(err);
  }
);

const secondBoardName = 'Darkseid';

callback6(
  {
    boards,
    lists,
    cards,
    boardName: secondBoardName,
  },
  err => {
    console.error(err);
  }
);
