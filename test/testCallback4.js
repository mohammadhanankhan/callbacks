const boards = require('../data/boards.json');
const lists = require('../data/lists.json');
const cards = require('../data/cards.json');

const callback4 = require('../callback4');

const boardName = 'Thanos';
const listName = 'Mind';

callback4(
  {
    boards,
    lists,
    cards,
    boardName,
    listName,
  },
  err => {
    console.error(err);
  }
);

const secondBoardName = 'Darkseid';
const secondListName = 'Soul';

callback4(
  {
    boards,
    lists,
    cards,
    boardName: secondBoardName,
    listName: secondListName,
  },
  err => {
    console.error(err);
  }
);
