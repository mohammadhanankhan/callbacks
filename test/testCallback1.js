const boards = require('../data/boards.json');

const callback1 = require('../callback1');

const firstBoardId = 'mcu453ed';

callback1(boards, firstBoardId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});

const secondBoardId = 'abc122dc';

callback1(boards, secondBoardId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});
