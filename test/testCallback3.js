const cards = require('../data/cards.json');

const callback3 = require('../callback3');

const firstListId = 12343;

callback3(cards, firstListId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});

const secondListId = 'qwsa221';

callback3(cards, secondListId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});
