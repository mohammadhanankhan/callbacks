const boards = require('../data/boards.json');
const lists = require('../data/lists.json');
const cards = require('../data/cards.json');

const callback5 = require('../callback5');

const boardName = 'Thanos';
const listNames = ['Mind', 'Space'];

callback5(
  {
    boards,
    lists,
    cards,
    boardName,
    listNames,
  },
  err => {
    console.error(err);
  }
);

const secondBoardName = 'Darkseid';
const secondListNames = ['Telekinesis', 'Flight'];

callback5(
  {
    boards,
    lists,
    cards,
    boardName: secondBoardName,
    listNames: secondListNames,
  },
  err => {
    console.error(err);
  }
);
