const lists = require('../data/lists.json');

const callback2 = require('../callback2');

const firstBoardId = 1212;

callback2(lists, firstBoardId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});

const secondBoardId = 'mcu453ed';

callback2(lists, secondBoardId, (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
});
