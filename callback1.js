/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/
const callback1 = function (boards, boardId, callback) {
  if (
    Array.isArray(boards) &&
    typeof boardId === 'string' &&
    typeof callback === 'function'
  ) {
    setTimeout(() => {
      const specificBoard = boards.find(board => board.id === boardId);

      if (specificBoard === undefined) {
        callback(new Error('Not found!'), null);
      } else {
        callback(null, specificBoard);
      }
    }, 2 * 1000);
  } else {
    callback(new Error('Invalid input!'), null);
  }
};

module.exports = callback1;
