/* 
	Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
*/

const callback2 = function (lists, boardId, callback) {
  if (
    typeof lists === 'object' &&
    lists !== null &&
    typeof boardId === 'string' &&
    typeof callback === 'function'
  ) {
    setTimeout(() => {
      const specificList = lists[boardId];

      if (specificList === undefined) {
        callback(new Error('Not found!'), null);
      } else {
        callback(null, specificList);
      }
    }, 2 * 1000);
  } else {
    callback(new Error('Invalid input!'), null);
  }
};
module.exports = callback2;
