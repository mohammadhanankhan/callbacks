/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const callback3 = function (cards, listId, callback) {
  if (
    typeof cards === 'object' &&
    cards !== null &&
    typeof listId === 'string' &&
    typeof callback === 'function'
  ) {
    setTimeout(() => {
      const specificCards = cards[listId];

      if (specificCards === undefined) {
        callback(new Error('Not found!'), null);
      } else {
        callback(null, specificCards);
      }
    }, 2 * 1000);
  } else {
    callback(new Error('Invalid input!'), null);
  }
};
module.exports = callback3;
